public class Board
{
    private Die value1;
	private Die value2;
	private boolean [] tiles;
	
	//Constructor
	public Board()
	{
		this.value1 = new Die();
		this.value2 = new Die();
		this.tiles = new boolean[12];
	}
	//toString method
	public String toString()
	{ String arrTiles = " ";
	  int count = 1;
		for( int i=0; i<this.tiles.length;i++)
		{
			if(this.tiles[i]==true)
		       arrTiles += "X"+" ";
		    else
			   arrTiles += Integer.toString(count)+" ";
		   count++;
		}
	   return "Values of the modified board are"+arrTiles;
	}
	//Instance Methods 
	public boolean playATurn()
	{
		value1.roll();
		value2.roll();
		System.out.println(value1);
		System.out.println(value2);
		
		boolean finalValue = false;
		
		int sumOfDice = (value1.getFaceValue())+(value2.getFaceValue());
		
		if((this.tiles[sumOfDice-1]==true) && (this.tiles[value1.getFaceValue()-1]==true)&& (this.tiles[value2.getFaceValue()-1]==true))
	    {
			System.out.println("All the tiles for these values are already shut!");
			finalValue=true;
		}
		if(this.tiles[sumOfDice-1]==false)
		{
			this.tiles[sumOfDice-1]=true;
			System.out.println("Closing tile equal to sum: "+(sumOfDice));
			finalValue=false;
		}
		if(this.tiles[value1.getFaceValue()-1]==false)
		{
			this.tiles[value1.getFaceValue()-1]=true;
			System.out.println("Closing tile with the same value as die one: "+value1.getFaceValue());
			finalValue=false;
		}
		if(this.tiles[value2.getFaceValue()-1]==false)
		{
			this.tiles[value2.getFaceValue()-1]=true;
			System.out.println("Closing tile with the same value as die two: "+value2.getFaceValue());
			finalValue=false;
		}
		
		return finalValue;
	}
}
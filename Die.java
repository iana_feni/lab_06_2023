import java.util.Random;
public class Die{
   
    private int faceValue;
	private Random random;
	
	//Contructor
	public Die(){
	
	this.random = new Random();
	this.faceValue = 1;
	
	}
	//Get method
	public int getFaceValue()
	{
		 return this.faceValue;
	}
	
	//Assign a random number to faceValue
	public void roll()
	{
		this.faceValue = random.nextInt(6)+1;
	}
	
	//toString method
	public String toString()
	{
		return "Value of the face of the dice:"+this.faceValue;
	}







}